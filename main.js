$(document).ready(function() {
	var blackPieces = {
		king: [85],
		queen: [84],
		rook: [81, 88],
		knight: [82, 87],
		bishop: [83, 86],
		pawn: [71, 72, 73, 74, 75, 76, 77, 78]
	};

	var whitePieces = {
		king: [15],
		queen: [14],
		rook: [11, 18],
		knight: [12, 17],
		bishop: [13, 16],
		pawn: [21, 22, 23, 24, 25, 26, 27, 28]
	};
	var piceStrings = ['', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

	var rookStates = [true, true, true, true];
	var bishopStates = [true, true, true, true];
	var knightStates = [true, true, true, true, true, true, true, true];

	var colorMove = 'white';
	var pieceSelected = '';
	var pieceSelectedID = '';
	var moveState = true;

	var countMoves = 0;

	createBoard();
	addPieces();

	function createBoard() {
		for (var i = 8; i >= 1; i--) {
			$(".board").append('<div id="' + i + '"></div>');

			for (var j = 1; j <= 8; j++) {
				$("#" + i).append('<div id="' + i + '' + j + '"></div>');
			}
		}
	}

	function addPieces() {
		for (var key in whitePieces) {
			for (var i = 0; i < whitePieces[key].length; i++) {
				$('#' + whitePieces[key][i]).addClass(key + '-white');
			}
		}
		for (var key in blackPieces) {
			for (var j = 0; j < blackPieces[key].length; j++) {
				$('#' + blackPieces[key][j]).addClass(key + '-black');
			}
		}
	}



	function pawn(x, y, color) {
		var enemyColor = color == 'black' ? 'white' : 'black';
		if (color == 'white') {
			if ($('#' + (x + 1) + y).hasClass('')) {
				$('#' + (x + 1) + y).addClass('green');
			}
			if (!$('#' + (x + 1) + (y - 1)).hasClass('') && $('#' + (x - 1) + (y - 1)).length) {
				if ($('#' + (x + 1) + (y - 1)).attr('class').split('-')[1] == enemyColor) {
					$('#' + (x + 1) + (y - 1)).addClass('red');
				}
			}
			if (!$('#' + (x + 1) + (y + 1)).hasClass('') && $('#' + (x - 1) + (y + 1)).length) {
				if ($('#' + (x + 1) + (y + 1)).attr('class').split('-')[1] == enemyColor) {
					$('#' + (x + 1) + (y + 1)).addClass('red');
				}
			}
		} else {
			if ($('#' + (x - 1) + y).hasClass('')) {
				$('#' + (x - 1) + y).addClass('green');
			}

			if (!$('#' + (x - 1) + (y - 1)).hasClass('') && $('#' + (x + 1) + (y - 1)).length) {
				if ($('#' + (x - 1) + (y - 1)).attr('class').split('-')[1] == enemyColor) {
					$('#' + (x - 1) + (y - 1)).addClass('red');
				}
			}
			if (!$('#' + (x - 1) + (y + 1)).hasClass('') && $('#' + (x + 1) + (y + 1)).length) {
				if ($('#' + (x - 1) + (y + 1)).attr('class').split('-')[1] == enemyColor) {
					$('#' + (x - 1) + (y + 1)).addClass('red');
				}
			}
		}
	}

	function king(x, y, color) {
		rookStates = [true, true, true, true];
		bishopStates = [true, true, true, true];
		addCombs('#' + (x + 1) + (y + 1), bishopStates, 0, color);
		addCombs('#' + (x - 1) + (y + 1), bishopStates, 1, color);
		addCombs('#' + (x - 1) + (y - 1), bishopStates, 2, color);
		addCombs('#' + (x + 1) + (y - 1), bishopStates, 3, color);
		addCombs('#' + (x - 1) + (y), rookStates, 0, color);
		addCombs('#' + (x) + (y - 1), rookStates, 1, color);
		addCombs('#' + (x + 1) + (y), rookStates, 2, color);
		addCombs('#' + (x) + (y + 1), rookStates, 3, color);
	}

	function knight(x, y, color) {
		knightStates = [true, true, true, true, true, true, true, true];
		addCombs('#' + (x + 2) + (y + 1), knightStates, 0, color);
		addCombs('#' + (x - 2) + (y - 1), knightStates, 1, color);
		addCombs('#' + (x - 2) + (y + 1), knightStates, 2, color);
		addCombs('#' + (x + 2) + (y - 1), knightStates, 3, color);
		addCombs('#' + (x + 1) + (y + 2), knightStates, 4, color);
		addCombs('#' + (x - 1) + (y - 2), knightStates, 5, color);
		addCombs('#' + (x - 1) + (y + 2), knightStates, 6, color);
		addCombs('#' + (x + 1) + (y - 2), knightStates, 7, color);

	}

	function rook(x, y, color) {
		rookStates = [true, true, true, true];
		for (var i = 1; i <= 8; i++) {
			addCombs('#' + (x - i) + (y), rookStates, 0, color);
			addCombs('#' + (x) + (y - i), rookStates, 1, color);
			addCombs('#' + (x + i) + (y), rookStates, 2, color);
			addCombs('#' + (x) + (y + i), rookStates, 3, color);
		}
	}

	function bishop(x, y, color) {
		bishopStates = [true, true, true, true];
		for (var i = 1; i <= 8; i++) {
			addCombs('#' + (x + i) + (y + i), bishopStates, 0, color);
			addCombs('#' + (x - i) + (y + i), bishopStates, 1, color);
			addCombs('#' + (x - i) + (y - i), bishopStates, 2, color);
			addCombs('#' + (x + i) + (y - i), bishopStates, 3, color);
		}
	}

	function queen(x, y, color) {
		bishop(x, y, color);
		rook(x, y, color);
	}

	function addCombs(elmSelect, states, idx, color) {
		if ($(elmSelect).length) {
			if ($(elmSelect).hasClass('') && states[idx]) {
				$(elmSelect).addClass('green');
			} else {
				if (states[idx] && $(elmSelect).attr('class').split('-')[1] !== color) {
					$(elmSelect).addClass('red');
				}
				states[idx] = false;
			}
		}
	}

	$('body').on('click', '.board div div', function() {


		if (moveState && !$(this).hasClass('') && $(this).attr('class').split('-')[1] == colorMove) {
			var name = $(this).attr('class').split('-')[0];
			var color = $(this).attr('class').split('-')[1];
			var column = Number($(this).attr('id').toString().split('')[0]);
			var row = Number($(this).attr('id').toString().split('')[1]);

			if (name == 'rook') {
				rook(column, row, color);
			}
			if (name == 'bishop') {
				bishop(column, row, color);
			}
			if (name == 'queen') {
				queen(column, row, color);
			}
			if (name == 'king') {
				king(column, row, color);
			}
			if (name == 'knight') {
				knight(column, row, color);
			}
			if (name == 'pawn') {
				pawn(column, row, color);
			}
			pieceSelected = $(this).attr('class');
			pieceSelectedID = $(this).attr('id');
			firstPieceNumber = $(this).attr('id').toString().split('')[0];
			firstPieceLetter = piceStrings[Number($(this).attr('id').toString().split('')[1])];

			moveState = false;
		} else {

			secondPieceNumber = $(this).attr('id').toString().split('')[0];
			secondPieceLetter = piceStrings[Number($(this).attr('id').toString().split('')[1])];

			var actionType = $(this).hasClass('red') ? ' x ' : ' - ';

			if ($(this).hasClass('red') || $(this).hasClass('green')) {
				countMoves++;
				$(this).removeAttr('class').addClass(pieceSelected);
				$('#' + pieceSelectedID).removeAttr('class');
				colorMove = colorMove == 'white' ? 'black' : 'white';

				$('.playerTurn').text(colorMove + ' turn');
				$('.move-history').show();
				$('.history').append('<span>' + firstPieceNumber + firstPieceLetter + actionType + secondPieceNumber + secondPieceLetter + '<span>');
			}

			$(".board div div").removeClass('red green');
			pieceSelected = '';
			pieceSelectedID = '';
			moveState = true;
		}
	});
});